# K9 Forms
A ReactJS application that interfaces with the Rescue Groups API 
to provide printable Rabies and Spay/Neuter forms

## Configuration

Create a `.env` file in the root of this folder. 

Fill out the following values
```
RG_TOKEN=your rescue groups token
RG_TOKEN_HASH=your rescue groups token hash
REDIS_HOST=redis
REDIS_PORT=6379
```

## Run
To run the application execute the following command: `docker-compose up`

You can then access the website at http://localhost:8000

## Deploy
To create a docker image and deploy it to docker hub run `./deploy.sh`