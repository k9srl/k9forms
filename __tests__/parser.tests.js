const dataParser = require('../server/services/dataParser');

test('parses rabies description string', () => {
    let data = "exp: 2/14/2019\r\nmfg: Rabvac 1\r\nserial: 4130254A"
    let sut = dataParser.rabiesMetaParser(data);
    expect(sut.expiration).toBe('2/14/2019');
    expect(sut.manufacturer).toBe('Rabvac 1');
    expect(sut.serial).toBe('4130254A');
});

test('parses rabies description string non trimmed', () => {
    let data = "exp : 2/14/2019\r\nmfg : Rabvac 1\r\nserial : 4130254A"
    let sut = dataParser.rabiesMetaParser(data);
    expect(sut.expiration).toBe('2/14/2019');
    expect(sut.manufacturer).toBe('Rabvac 1');
    expect(sut.serial).toBe('4130254A');
});

test('parses corner case expiration', () => {
    let data = "expiration: 2/14/2019\r\nmfg: Rabvac 1\r\nserial: 4130254A"
    let sut = dataParser.rabiesMetaParser(data);
    expect(sut.expiration).toBe('2/14/2019');
});

test('parses corner case expires', () => {
    let data = "expires: 2/14/2019\r\nmfg: Rabvac 1\r\nserial: 4130254A"
    let sut = dataParser.rabiesMetaParser(data);
    expect(sut.expiration).toBe('2/14/2019');
});

test('parses corner case manufacturer', () => {
    let data = "exp: 2/14/2019\r\nmanufacturer: Rabvac 1\r\nserial: 4130254A"
    let sut = dataParser.rabiesMetaParser(data);
    expect(sut.manufacturer).toBe('Rabvac 1');
});

test('parses corner case manf', () => {
    let data = "exp: 2/14/2019\r\nmanufacturer: Rabvac 1\r\nserial: 4130254A"
    let sut = dataParser.rabiesMetaParser(data);
    expect(sut.manufacturer).toBe('Rabvac 1');
});

test('parses corner case serial #', () => {
    let data = "exp: 2/14/2019\r\nmfg: Rabvac 1\r\nserial #: 4130254A"
    let sut = dataParser.rabiesMetaParser(data);
    expect(sut.serial).toBe('4130254A');
});

test('parses corner case serial num', () => {
    let data = "exp: 2/14/2019\r\nmfg: Rabvac 1\r\nserial num: 4130254A"
    let sut = dataParser.rabiesMetaParser(data);
    expect(sut.serial).toBe('4130254A');
});

test('parses corner case serial number', () => {
    let data = "exp: 2/14/2019\r\nmfg: Rabvac 1\r\nserial number: 4130254A"
    let sut = dataParser.rabiesMetaParser(data);
    expect(sut.serial).toBe('4130254A');
});