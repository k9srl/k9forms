import { Util } from '../server/services/util';

test('altered female expect spay', () => {
    let altered = "Yes";
    let gender = "Female";
    let sut = Util.getAlteredText(altered, gender);
    expect(sut).toBe("(Spayed)");
});

test('altered male expect neuter', () => {
    let altered = "Yes";
    let gender = "Male";
    let sut = Util.getAlteredText(altered, gender);
    expect(sut).toBe("(Neutered)");
});

test('non altered male expect blank', () => {
    let altered = "No";
    let gender = "Male";
    let sut = Util.getAlteredText(altered, gender);
    expect(sut).toBe("");
});


test('non altered female expect blank', () => {
    let altered = "No";
    let gender = "Female";
    let sut = Util.getAlteredText(altered, gender);
    expect(sut).toBe("");
});