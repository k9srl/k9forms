import * as React from "react";
import { Route, Switch, Link } from 'react-router-dom';
import DogList from "./DogList";
import RabiesForm from "./RabiesForm";
import Navigation from "./Navigation";
import AlteredForm from "./AlteredForm";
import Admin from "./Admin";

export class App extends React.Component<{}> {

  render() {
    const App = () => (
      <div>
        <Switch>
          <Route exact path='/' component={DogList}/>
          <Route path='/forms/rabies/:id' component={RabiesForm}/>
          <Route path='/forms/altered/:id' component={AlteredForm}/>
          <Route path='/admin' component={Admin}/>
        </Switch>
      </div>
    )
    return (
      <div>
        <Navigation />
        <main role="main" className="container">
          <Switch>
            <App/>
          </Switch>
        </main>
      </div>
    );
  }
}