import * as React from "react";
import { Table, Button } from 'reactstrap';
import { IAlteredFormProps, IAlteredFormState, IAlteredFormDispatcherProps } from '../../models/IAlteredForm';
import { FormHeader } from './FormHeader';
import { FormSignature } from './FormSignature';
import * as html2canvas from 'html2canvas'
import * as jsPDF from 'jspdf'

import "react-datepicker/dist/react-datepicker.css";

class AlteredForm extends React.Component<IAlteredFormProps, IAlteredFormState> {
    constructor(props: IAlteredFormProps & IAlteredFormDispatcherProps) {
        super(props);

        this.state = {
            dog: {
                name: "Name",
                bornOn: "Born On",
                color: "Color",
                gender: "Gender",
                id: 1,
                vaccinatedOn: "Vaccinated On",
                breed: "Breed",
                altered: "N/A"
            },
            procedureDate: new Date()
        }

        this.goBack = this.goBack.bind(this);
    }

    goBack(){
        this.props.history.goBack();
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        fetch(`/dogs/${id}`)
            .then(response => {
                return response.json();
            }).then(data => {
                this.setState({
                    dog: data,
                });
            });

        
    }

    export = (isDouble: Boolean):void => {
        let $inputs = document.querySelector('input');
        $inputs.classList.add('print');

        html2canvas(document.querySelector("#altered-form")).then(canvas => {
            var imgData = canvas.toDataURL(
                'image/png');              
            var doc = new jsPDF('p','in','letter');

            doc.addImage(imgData, 'PNG', 0.25, 0.25, 8, 4.5);

            if (isDouble) {
                doc.addImage(imgData, 'PNG', 0.25, 5.5, 8, 4.5);
            }

            doc.autoPrint();
            window.open(doc.output('bloburl'), '_blank');    
        }).then(() => $inputs.classList.remove('print'));
    }

    render() {
        return (
            <div>
                <div className="form-toolbar">
                    <Button variant="primary" onClick={this.goBack}>
                        <i className="fas fa-arrow-left"></i>
                        &nbsp; Back
                    </Button>
                    <Button onClick={() => this.export(false)}>
                        <i className="far fa-file"></i>
                        &nbsp; Print Single
                    </Button>
                    <Button onClick={() => this.export(true)}>
                        <i className="far fa-copy"></i>
                        &nbsp; Print Double
                    </Button>
                </div>
                <div id="altered-form">
                    <FormHeader formName="Spay/Neuter" />
                    <br />
                    <div className="row">
                        <div className="col-md-12" style={{textAlign: 'center'}}>
                            <p>
                            This document certifies that the dog listed below has been spay/neutered
                            </p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 offset-md-1">
                            <Table borderless className="k9-form">
                                <tbody>
                                    <tr>
                                        <th scope="row">Dog</th>
                                        <td> { this.state.dog.name } ({ this.props.match.params.id })</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Breed</th>
                                        <td>{ this.state.dog.breed }</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Color</th>
                                        <td>{ this.state.dog.color }</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Gender</th>
                                        <td>{ this.state.dog.gender }</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Born</th>
                                        <td>{ this.state.dog.bornOn }</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                    </div>
 
                    <FormSignature isEditable={true} />
                </div>
            </div>
        )
    }
}

export default AlteredForm;