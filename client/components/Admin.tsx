import * as React from 'react';
import { Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';
import { IAdminProps, IAdminState, IAdminDispatcherProps } from '../../models/IAdmin';
import axios from 'axios';

class Admin extends React.Component<IAdminProps,IAdminState> {
    constructor(props: IAdminProps & IAdminDispatcherProps) {
        super(props);

        this.state = {
            surgeon: "",
            showSuccess: false,
            showError: false
        }
    }

    componentDidMount() {
        fetch(`/config`)
            .then(response => {
                return response.json();
            }).then(data => {
                this.setState({
                    surgeon: data.surgeon
                });
            });
    }

    onSurgeonChange = (event:React.ChangeEvent<HTMLInputElement>):void => {
        this.setState({
            surgeon: event.target.value
        });
    }

    onSubmit = (event:any):void => {
        var data = {
            surgeon: this.state.surgeon
        }
        axios.post('/config', data)
            .then((response) => {
                this.onAlert(true);
            })
            .catch((err) =>{
                console.log(err);
                this.onAlert(false);
            });
    }

    onAlert = (isSuccessful:boolean):void => {
        if (isSuccessful) {
            this.setState({
                showSuccess: true
            })

            setTimeout(() => {
                this.setState({
                    showSuccess: false
                })
            }, 3000)
        }
        else {
            this.setState({
                showError: true
            })

            setTimeout(() => {
                this.setState({
                    showError: false
                })
            }, 5000)
        }

    }

    render() {
        return (
            <div className="topPad">
                <Alert color="success" style={{ display: this.state.showSuccess ? "block" : "none" }}>
                    Update Successful!
                </Alert>
                <Alert color="danger"  style={{ display: this.state.showError ? "block" : "none" }}>
                    Update Failed!
                </Alert>
                <h2>Administration</h2>
                <Form>
                    <FormGroup>
                        <Label for="inputSurgeon">Surgeon</Label>
                        <Input type="text" name="surgeon" id="inputSurgeon" value={this.state.surgeon} onChange={this.onSurgeonChange} />
                    </FormGroup>
                    <Button onClick={this.onSubmit} color="primary">Update</Button>
                </Form>
            </div> 
        )
    }
}

export default Admin