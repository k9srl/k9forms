import * as React from "react";
import {
    Navbar,
    NavbarToggler
} from 'reactstrap';
import { Link } from "react-router-dom"


class Navigation extends React.Component<{}> {

    adminClick = (e:any) => {
        
    }

    render() {

        return (
            <div>
                <Navbar color="dark" dark expand="md">
                    <Link to={'/'} className="navbar-brand">K-9 Forms</Link>
                    <NavbarToggler />
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link to={'/admin'} className="nav-link">
                                <i className="fas fa-cog"></i>
                            </Link>
                        </li>
                    </ul>
                </Navbar>
            </div>
        );
    }
}

export default Navigation;