import * as React from "react";
import { Button, Table, Alert } from 'reactstrap';
import { IDogListProp, IDogListState } from '../../models/IDogList';
import { Link } from "react-router-dom"

class DogList extends React.Component<IDogListProp, IDogListState> {
    constructor(props: IDogListProp) {
        super(props);

        this.state = {
            dogs: [],
            selectedDog: null
        }
    }

    componentDidMount() {
        fetch(`/dogs`)
            .then(response => {
                return response.json();
            }).then(data => {
                this.setState({
                    dogs: data,
                });
            });
    }

    render() {
        return (
            <div>
            <Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Dog ID</th>
                        <th>Dog Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.dogs.length <= 0 &&
          
                        <span className='no-dog-found'>
                            No available dogs found
                        </span>
                    }
                    {this.state.dogs.map((listValue, index) => {
                        return (
                            <tr key={index}>
                                <th scope="row">{index+1}</th>
                                <td>{listValue.id}</td>
                                <td>{listValue.name}</td>
                                <td>
                                    <Link to={{ pathname: `/forms/altered/${listValue.id}` }}>
                                        <Button color="primary">Spay/Neuter</Button>{' '}
                                    </Link>
                                    <Link to={{ pathname: `/forms/rabies/${listValue.id}` }}>
                                        <Button color="info">Rabies</Button>{' '}
                                    </Link>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
          </div>
        )
    }
}


export default DogList;