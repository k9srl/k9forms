import * as React from "react";
import { Table } from 'reactstrap';
import { IFormSignatureProps, IFormSignatureState, IFormSignatureDispatcherProps } from "../../models/IFormSignature";
import DatePicker from "react-datepicker";

export class FormSignature extends React.Component<IFormSignatureProps, IFormSignatureState> {
    constructor(props: IFormSignatureProps & IFormSignatureDispatcherProps) {
        super(props);

        this.state = {
            surgeon: "",
            currentDateTime: new Date()
        }
    }

    toDateTimeString = (today:Date):string => {
        let date =  `${(today.getMonth() + 1)}/${today.getDate()}/${today.getFullYear()}`;
        return date;
    }

    handleChange = (date:Date):void => {
        this.setState({
            currentDateTime: date
        });
      };

    componentDidMount() {
        fetch(`/config`)
            .then(response => {
                return response.json();
            }).then(data => {
                this.setState({
                    surgeon: data.surgeon
                });
            });
    }
    render() {
        //David Smith, D.V.M., #6901005915

        const isEditable = this.props.isEditable;
        let date;

        if (isEditable) {
            date = <DatePicker
                selected={this.state.currentDateTime} 
                onChange={this.handleChange} />
        }
        else{
            date = this.toDateTimeString(this.state.currentDateTime)
        }

        return (
            <div className="col-md-12">
                <div className="col-md-9 offset-md-1">
                    <Table borderless>
                        <tbody>
                            <tr>
                                <td className="signature"></td>
                                <td></td>
                                <td className="signature">    
                                    {date}
                                </td>
                            </tr>
                            <tr>
                                <td>{this.state.surgeon}</td>
                                <td></td>
                                <td>Date</td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
            </div>
        );
    }
}

export default FormSignature;