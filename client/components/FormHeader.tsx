import * as React from "react";
import { IFormHeaderProps, IFormHeaderState } from '../../models/IFormHeader';

export class FormHeader extends React.Component<IFormHeaderProps, IFormHeaderState> {
    constructor(props: IFormHeaderProps) {
        super(props);
    }
    render() {

        return (
            <div className="row k9-header">
                <div className="col-md-12">
                    <h1>K-9 Stray Rescue League {this.props.formName} Certification</h1>
                    <h4>2120 Metamora Rd.</h4>
                    <h4>Oxford MI, 48371</h4>
                    <h4>248-628-0435</h4>
                </div>
            </div>
        );
    }
}

export default FormHeader;