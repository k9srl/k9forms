import * as React from "react";
import { Button, Table } from 'reactstrap';
import { IRabiesFormProps, IRabiesFormState, IRabiesFormDispatcherProps } from '../../models/IRabiesForm';
import { FormHeader } from './FormHeader';
import { FormSignature } from './FormSignature';
import { Util } from '../../server/services/util';
import * as html2canvas from 'html2canvas'
import * as jsPDF from 'jspdf'

class RabiesForm extends React.Component<IRabiesFormProps, IRabiesFormState> {
    
    constructor(props: IRabiesFormProps & IRabiesFormDispatcherProps) {
        super(props);

        this.state = {
            dog: {
                name: "Name",
                bornOn: "Born On",
                color: "Color",
                gender: "Gender",
                id: 1,
                vaccinatedOn: "Vaccinated On",
                breed: "Breed",
                altered: "N/A"
            },
            dogId: "1",
            rabiesInfo: {
                expiresOn: "N/A",
                id: "1",
                manufacturer: "N/A",
                serialNumber: "N/A",
                vaccinatedOn: "N/A",
                dueOn: "N/A"
            }
        }

        this.goBack = this.goBack.bind(this);
    }
    goBack(){
        this.props.history.goBack();
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        fetch(`/dogs/${id}`)
            .then(response => {
                return response.json();
            }).then(data => {
                this.setState({
                    dog: data,
                });
            });
        fetch(`/dogs/${id}/rabiesInfo`)
            .then(response => {
                return response.json();
            }).then(data => {
                this.setState({
                    rabiesInfo: data,
                });
            });

        
    }

    export = (isDouble: Boolean):void => {
        let $inputs = document.querySelector('input');
        $inputs.classList.add('print');

        html2canvas(document.querySelector("#rabies-form")).then(canvas => {
            var imgData = canvas.toDataURL(
                'image/png');              
            var doc = new jsPDF('p','in','letter');

            doc.addImage(imgData, 'PNG', 0.25, 0.25, 8, 4.75);

            if (isDouble) {
                doc.addImage(imgData, 'PNG', 0.25, 5.5, 8, 4.75);
            }

            doc.autoPrint();
            window.open(doc.output('bloburl'), '_blank');
        }).then(() => $inputs.classList.remove('print'));
    }

    render() {
        return (
            <div>
                <div className="form-toolbar">
                    <Button variant="primary" onClick={this.goBack}>
                        <i className="fas fa-arrow-left"></i>
                        &nbsp; Back
                    </Button>
                    <Button onClick={() => this.export(false)}>
                        <i className="far fa-file"></i>
                        &nbsp; Print Single
                    </Button>
                    <Button onClick={() => this.export(true)}>
                        <i className="far fa-copy"></i>
                        &nbsp; Print Double
                    </Button>
                </div>
                
                <div id="rabies-form">
                    <FormHeader formName="Rabies" />
                    <br />
                    <br />
                    <div className="row">
                        <div className="col-md-4 offset-md-1">
                            <Table borderless className="k9-form">
                                <tbody>
                                    <tr>
                                        <th scope="row">Dog</th>
                                        <td> { this.state.dog.name } ({ this.props.match.params.id })</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Breed</th>
                                        <td>{ this.state.dog.breed }</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Color</th>
                                        <td>{ this.state.dog.color }</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Gender</th>
                                        <td>{ this.state.dog.gender } { Util.getAlteredText(this.state.dog.altered, this.state.dog.gender) }</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Born</th>
                                        <td>{ this.state.dog.bornOn }</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                        <div className="col-md-4 offset-md-1">
                            <Table borderless className="k9-form">
                                <tbody>
                                    <tr>
                                        <th scope="row">Rabies Serial #</th>
                                        <td>{ this.state.rabiesInfo.serialNumber }</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Manufacturer</th>
                                        <td>{ this.state.rabiesInfo.manufacturer }</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Mfg Expiration</th>
                                        <td>{ this.state.rabiesInfo.expiresOn }</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Vaccine Due Next On</th>
                                        <td>{ this.state.rabiesInfo.dueOn }</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                        
                        <FormSignature isEditable={true} />
                        
                    </div>
                </div>
            </div>
        )
    }
}

export default RabiesForm;