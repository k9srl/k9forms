#!/bin/bash

REPO="wsharp007/k9forms"

echo "Building Docker Image..."
docker build -t $REPO .

echo "Pushing Docker Image..."
docker push $REPO