export interface IAdminState {
    surgeon: string,
    showSuccess: boolean,
    showError: boolean
}

export interface IAdminProps {

}

export interface IAdminDispatcherProps {
    // Add your dispatcher properties here
  }