import { IDog } from './IDog';
import {RouteComponentProps} from 'react-router-dom'

export interface IAlteredFormState {
    dog: IDog,
    procedureDate: Date
}

export interface IAlteredFormRouterProps {
    id: string;
}

export interface IAlteredFormProps extends RouteComponentProps<IAlteredFormRouterProps> {

}

export interface IAlteredFormDispatcherProps {
    // Add your dispatcher properties here
  }