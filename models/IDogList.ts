import { IDog } from './IDog';

export interface IDogListState {
    dogs: Array<IDog>;
    selectedDog: string;
}

export interface IDogListProp {
}