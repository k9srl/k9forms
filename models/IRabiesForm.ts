import { IDog } from './IDog';
import { IRabiesInfo } from './IRabiesInfo'
import {RouteComponentProps} from 'react-router-dom'

export interface IRabiesFormState {
    dogId: string,
    dog: IDog,
    rabiesInfo: IRabiesInfo
}

export interface IRabiesFormRouterProps {
    id: string;
}

export interface IRabiesFormProps extends RouteComponentProps<IRabiesFormRouterProps> {

}

export interface IRabiesFormDispatcherProps {
    // Add your dispatcher properties here
  }