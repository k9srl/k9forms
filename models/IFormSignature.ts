export interface IFormSignatureState {
    surgeon: string,
    currentDateTime: Date
}

export interface IFormSignatureProps {
    isEditable: boolean

}

export interface IFormSignatureDispatcherProps {
    // Add your dispatcher properties here
  }