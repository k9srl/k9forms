FROM node:10-alpine as base
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
EXPOSE 8000

RUN apk add --no-cache git 
ENV NODE_ENV development
COPY package.json package-lock.json ./
RUN npm install
COPY .babelrc nodemon.json webpack.config.js tsconfig.json ./
COPY client ./client
COPY server ./server
COPY models ./models
COPY bin ./bin
RUN npm run build
CMD ["npm", "start"]
