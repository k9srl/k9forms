var express = require('express');

const homeController = require('../controllers/homeController');
const loginController = require('../controllers/loginController');
const dogController = require('../controllers/dogController');
const configController = require('../controllers/configController');

var router = express.Router();

/// HOME
router.get('/', homeController.index);

/// LOGIN
router.get('/login', loginController.login);

/// DOGS
router.get('/dogs', dogController.dogList);
router.get('/dogs/:id', dogController.dog);
router.get('/dogs/:id/rabiesInfo', dogController.rabiesInfo);

/// CONFIG
router.get('/config', configController.getConfig);
router.post('/config', configController.setConfig);

/// HEALTH CHECK
router.get('/healthcheck', async function(req, res) {
    res.status(200).send("I'm alive!");
});

module.exports = router;