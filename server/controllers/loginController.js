const loginService = require('../services/loginService');

exports.login = async function(req, res) {
  let username = req.query.username;
  let password = req.query.password;

  if (!username || !password) {
    res.send('You must specify a username and password query param')
    return;
  }

  let result = await loginService.login(username, password);
  console.log(result);
  res.send(result);
}