const configService = require('../services/configService');

exports.getConfig = async function(req, res) {
    var surgeon = await configService.getSurgeon().catch((err) => {
        res.status(500);
        res.send(err);
    });

    res.json({ surgeon: surgeon});
}

exports.setConfig = async function(req, res) {
    let value = req.body.surgeon;
    await configService.setSurgeon(value)
        .then((response) => {
            res.status(200);
            res.send();
        })
        .catch((err) => {
            res.status(500);
            res.send(err);
        });
}