const animalService = require('../services/animalService');
const config = require('../config');
const parser = require('../services/dataParser');

exports.dogList = async function(req, res) {
    let {
        page,
        pageSize
      } = req.query;
    
      if (!config.auth.token || !config.auth.tokenHash) {
        res.send('You must define token and tokenHash in your config file')
        return;
      }
    
      if (!page) {
        page = 1;
      }
    
      if (!pageSize) {
        pageSize = 100;
      }
    
      let animal = await animalService.getAllAvailable(config.auth, page, pageSize);
      let result = Object.values(animal).map((x) => {
        return {
          id: x.animalID,
          name: x.animalName,
          breed: x.animalBreed,
          color: x.animalColor,
          gender: x.animalSex,
          bornOn: x.animalBirthdate,
          altered: x.animalAltered
        }
      });
    
    
      res.send(result);
}

exports.dog = async function(req, res) {
    let animalId = req.params.id;

    if (!animalId) {
        res.send('You must specify and animalId');
    }

    let animal = await animalService.getDogById(config.auth, animalId);  
    let result = Object.values(animal).map((x) => {
        return {
          id: x.animalID,
          name: x.animalName,
          breed: x.animalBreed,
          color: x.animalColor,
          gender: x.animalSex,
          bornOn: x.animalBirthdate,
          altered: x.animalAltered
        }
    });

    res.send(result[0]);
}

exports.rabiesInfo = async function(req, res) {
    let animalId = req.params.id;

    if (!animalId) {
        res.send('You must specify and animalId');
    }

    let rabiesInfo = await animalService.getRabiesInfo(config.auth, animalId);
    let result = Object.values(rabiesInfo).map((x) => {

        let rabiesDetail = parser.rabiesMetaParser(x.journalEntryComment);

        return {
          id: x.journalEntryID,
          vaccinatedOn: x.journalEntryDate,
          dueOn: x.journalEntryDueDate,
          expiresOn: rabiesDetail.expiration,
          serialNumber: rabiesDetail.serial,
          manufacturer: rabiesDetail.manufacturer
        }
    });

    res.send(result[0]);
}