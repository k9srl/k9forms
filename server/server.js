var express = require('express');
var router = require('./routes/routes.js')
var path = require('path');
var bodyParser = require('body-parser')
var app = express();

const webpack = require('webpack');
// eslint-disable-next-line global-require
const config = require('../webpack.config');
// eslint-disable-next-line global-require
const webpackDevMiddleware = require('webpack-dev-middleware');
// eslint-disable-next-line global-require
const webpackHotMiddleware = require('webpack-hot-middleware');
const compiler = webpack(config);
app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
    watchOptions: {
        poll: 1000,
    },
}));
app.use(webpackHotMiddleware(compiler));
app.use(bodyParser.json())

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../client'));
app.use(express.static(path.join(__dirname, '../client')));
app.use('/dist', express.static(path.join(__dirname, '../dist')));

app.use('/', router);

module.exports = app;