export module Util {
    export function getAlteredText(altered: string, gender: string) {
        let isAltered: boolean = altered.toLowerCase() == "yes";

        if (isAltered && gender.toLowerCase() === "female") {
            return "(Spayed)";
        }

        if (isAltered && gender.toLowerCase() === "male") {
            return "(Neutered)";
        }

        return "";
    }
}