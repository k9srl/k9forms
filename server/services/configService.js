const redis = require("redis");
const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
let isConnected = false;


client.on("ready", function() {
    isConnected = true;
});

client.on("error", function(error) {
  console.error(error);
  isConnected = false;
});

function configService() {
    const SURGEON_KEY = "cfg_surgeon";

    function getSurgeon() {
        return new Promise((resolve, reject) => {
            if (!isConnected) {
                reject("Redis down");
            }

            client.get(SURGEON_KEY, (err, res) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                else {
                   resolve(res); 
                }
            });
        })
        
    }
    function setSurgeon(surgeon) {
        return new Promise((resolve,reject) => {
            if (!isConnected) {
                reject("Redis down");
            }

            client.set(SURGEON_KEY, surgeon, (err, res) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                else {
                    resolve(res);
                }
            });
        })
        
    }

    return { getSurgeon, setSurgeon };
}

module.exports = configService();