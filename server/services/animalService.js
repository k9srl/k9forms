const axios = require('axios');

function animalService() {
    const API_URL = 'https://api.rescuegroups.org/http/v2.json';
    function getAllAvailable(tokenObj, page, pageSize) {
        return new Promise((resolve, reject) => {
            let resultStart = (page-1) * pageSize;
            let resultEnd = page * pageSize;

            axios.post(API_URL, {
                token: tokenObj.token,
                tokenHash: tokenObj.tokenHash,
                objectType: "animals",
                objectAction: "search",
                search: {
                    resultStart: resultStart,
                    resultLimit: resultEnd,
                    resultSort: "animalID",
                    resultOrder: "asc",
                    calcFoundRows: "Yes",
                    filters: [
                        {
                            fieldName: "animalStatus",
                            operation: "equal",
                            criteria: "Available"
                        },
                        {
                            fieldName: "animalSpecies",
                            operation: "equal",
                            criteria: "Dog"
                        }
                    ],
                    fields: [
                        "animalID",
                        "animalName",
                        "animalBreed",
                        "animalColor",
                        "animalSex",
                        "animalBirthdate",
                        "animalAltered"
                    ]
                }
            })
            .then((response) => {
                resolve(response.data.data);
            })
            .catch((error) => {
                reject(error);
            });
        })
    }

    function getRabiesInfo(tokenObj, animalId) {
        return new Promise((resolve, reject) => {

            axios.post(API_URL, {
                token: tokenObj.token,
                tokenHash: tokenObj.tokenHash,
                objectType: "animalsJournalEntries",
                objectAction: "search",
                search: {
                    resultStart: "0",
                    resultLimit: "100",
                    resultSort: "journalEntryID",
                    resultOrder: "asc",
                    filters: [
                        {
                            fieldName: "journalEntryAnimalID",
                            operation: "equals",
                            criteria: animalId
                        },
                        {
                            fieldName: "journalEntrytypeDescription",
                            operation: "equals",
                            criteria: "Rabies"
                        }
                    ],
                    filterProcessing: "0",
                    fields: [
                        "journalEntryID",
                        "journalEntryAnimalID",
                        "journalEntryDate",
                        "journalEntryEntrytypeID",
                        "journalEntrytypeDescription",
                        "journalEntrytypeCategoryName",
                        "journalEntryDueDate",
                        "journalEntryComment"
                    ]
                }
            })
            .then((response) => {
                resolve(response.data.data)
            })
            .catch((error) => {
                reject(error);
            })
        })
    }

    function getDogById(tokenObj, animalId) {
        return new Promise((resolve, reject) => {

            axios.post(API_URL, {
                token: tokenObj.token,
                tokenHash: tokenObj.tokenHash,
                objectType: "animals",
                objectAction: "search",
                search: {
                    resultStart: 0,
                    resultLimit: 2,
                    resultSort: "animalID",
                    resultOrder: "asc",
                    calcFoundRows: "Yes",
                    filters: [
                        {
                            fieldName: "animalStatus",
                            operation: "equal",
                            criteria: "Available"
                        },
                        {
                            fieldName: "animalSpecies",
                            operation: "equal",
                            criteria: "Dog"
                        },
                        {
                            fieldName: "animalID",
                            operation: "equal",
                            criteria: animalId
                        }
                    ],
                    fields: [
                        "animalID",
                        "animalName",
                        "animalBreed",
                        "animalColor",
                        "animalSex",
                        "animalBirthdate",
                        "animalAltered"
                    ]
                }
            })
            .then((response) => {
                resolve(response.data.data);
            })
            .catch((error) => {
                reject(error);
            });
       })
    }

    return { getAllAvailable, getRabiesInfo, getDogById };
}

module.exports = animalService();