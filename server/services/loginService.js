const axios = require('axios');

function loginService() {
    function login(username, password) {
        return new Promise((resolve, reject) => {
            axios.post('https://api.rescuegroups.org/http/v2.json', {
                username: username,
                password: password,
                accountNumber: "4547",
                action: "login"
            })
            .then((response) => {
                resolve(response.data.data);
            })
            .catch((error) => {
                reject(error);
            });
        })
    }

    return { login };
}

module.exports = loginService();