function dataParser() {

    const expOpt = ["exp", "expiration", "expires"];
    const mfgOpt = ["mfg", "manf", "manufacturer"];
    const serialOpt = ["serial", "serial #", "serial num", "serial number"];

    function rabiesMetaParser(metaString) {
        let result = {
            expiration: "",
            manufacturer: "",
            serial: ""
        }

        metaString
            .split('\r\n')
            .map(x => x.split(':'))
            .map(x => x.map(y => y.trim()))
            .reduce((map,obj) => { 
                if (expOpt.indexOf(obj[0].toLowerCase()) > -1) {
                    map.expiration = obj[1].trim();
                }
                if (mfgOpt.indexOf(obj[0].toLowerCase()) > -1) {
                    map.manufacturer = obj[1].trim();
                }
                if (serialOpt.indexOf(obj[0].toLowerCase()) > -1) {
                    map.serial = obj[1].trim();
                }
                return map;
            }, result);

        return result;
    }
    
    return { rabiesMetaParser };
}

module.exports = dataParser();